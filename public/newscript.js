// VARIABLES

const DEPARTMENTS = {
	urls: [
		'ASIA&bolum=ASIAN+STUDIES',
		'ASIA&bolum=ASIAN+STUDIES+WITH+THESIS',
		'ATA&bolum=ATATURK+INSTITUTE+FOR+MODERN+TURKISH+HISTORY',
		'AUTO&bolum=AUTOMOTIVE+ENGINEERING',
		'BM&bolum=BIOMEDICAL+ENGINEERING',
		'BIS&bolum=BUSINESS+INFORMATION+SYSTEMS',
		'CHE&bolum=CHEMICAL+ENGINEERING',
		'CHEM&bolum=CHEMISTRY',
		'CE&bolum=CIVIL+ENGINEERING',
		'COGS&bolum=COGNITIVE+SCIENCE',
		'CSE&bolum=COMPUTATIONAL+SCIENCE+%26+ENGINEERING',
		'CET&bolum=COMPUTER+EDUCATION+%26+EDUCATIONAL+TECHNOLOGY',
		'CMPE&bolum=COMPUTER+ENGINEERING',
		'INT&bolum=CONFERENCE+INTERPRETING',
		'CEM&bolum=CONSTRUCTION+ENGINEERING+AND+MANAGEMENT',
		'CCS&bolum=CRITICAL+AND+CULTURAL+STUDIES',
		'EQE&bolum=EARTHQUAKE+ENGINEERING',
		'EC&bolum=ECONOMICS',
		'EF&bolum=ECONOMICS+AND+FINANCE',
		'ED&bolum=EDUCATIONAL+SCIENCES',
		'CET&bolum=EDUCATIONAL+TECHNOLOGY',
		'EE&bolum=ELECTRICAL+%26+ELECTRONICS+ENGINEERING',
		'ETM&bolum=ENGINEERING+AND+TECHNOLOGY+MANAGEMENT',
		'ENV&bolum=ENVIRONMENTAL+SCIENCES',
		'ENVT&bolum=ENVIRONMENTAL+TECHNOLOGY',
		'XMBA&bolum=EXECUTIVE+MBA',
		'FE&bolum=FINANCIAL+ENGINEERING',
		'PA&bolum=FINE+ARTS',
		'FLED&bolum=FOREIGN+LANGUAGE+EDUCATION',
		'GED&bolum=GEODESY',
		'GPH&bolum=GEOPHYSICS',
		'GUID&bolum=GUIDANCE+%26+PSYCHOLOGICAL+COUNSELING',
		'HIST&bolum=HISTORY',
		'HUM&bolum=HUMANITIES+COURSES+COORDINATOR',
		'IE&bolum=INDUSTRIAL+ENGINEERING',
		'INCT&bolum=INTERNATIONAL+COMPETITION+AND+TRADE',
		'MIR&bolum=INTERNATIONAL+RELATIONS%3aTURKEY%2cEUROPE+AND+THE+MIDDLE+EAST',
		'MIR&bolum=INTERNATIONAL+RELATIONS%3aTURKEY%2cEUROPE+AND+THE+MIDDLE+EAST+WITH+THESIS',
		'INTT&bolum=INTERNATIONAL+TRADE',
		'INTT&bolum=INTERNATIONAL+TRADE+MANAGEMENT',
		'LS&bolum=LEARNING+SCIENCES',
		'LING&bolum=LINGUISTICS',
		'AD&bolum=MANAGEMENT',
		'MIS&bolum=MANAGEMENT+INFORMATION+SYSTEMS',
		'MATH&bolum=MATHEMATICS',
		'SCED&bolum=MATHEMATICS+AND+SCIENCE+EDUCATION',
		'ME&bolum=MECHANICAL+ENGINEERING',
		'MECA&bolum=MECHATRONICS+ENGINEERING+(WITHOUT+THESIS)',
		'BIO&bolum=MOLECULAR+BIOLOGY+%26+GENETICS',
		'PHIL&bolum=PHILOSOPHY',
		'PE&bolum=PHYSICAL+EDUCATION',
		'PHYS&bolum=PHYSICS',
		'POLS&bolum=POLITICAL+SCIENCE%26INTERNATIONAL+RELATIONS',
		'PRED&bolum=PRIMARY+EDUCATION',
		'PSY&bolum=PSYCHOLOGY',
		'YADYOK&bolum=SCHOOL+OF+FOREIGN+LANGUAGES',
		'SCED&bolum=SECONDARY+SCHOOL+SCIENCE+AND+MATHEMATICS+EDUCATION',
		'SPL&bolum=SOCIAL+POLICY+WITH+THESIS',
		'SOC&bolum=SOCIOLOGY',
		'SWE&bolum=SOFTWARE+ENGINEERING',
		'SWE&bolum=SOFTWARE+ENGINEERING+WITH+THESIS',
		'TRM&bolum=SUSTAINABLE+TOURISM+MANAGEMENT',
		'SCO&bolum=SYSTEMS+%26+CONTROL+ENGINEERING',
		'TRM&bolum=TOURISM+ADMINISTRATION',
		'WTR&bolum=TRANSLATION',
		'TR&bolum=TRANSLATION+AND+INTERPRETING+STUDIES',
		'TK&bolum=TURKISH+COURSES+COORDINATOR',
		'TKL&bolum=TURKISH+LANGUAGE+%26+LITERATURE',
		'LL&bolum=WESTERN+LANGUAGES+%26+LITERATURES'
	],
	codes: [
		'ASIA',
		'ASIA W/ THESIS',
		'ATA',
		'AUTO',
		'BM',
		'BIS',
		'CHE',
		'CHEM',
		'CE',
		'COGS',
		'CSE',
		'CET',
		'CMPE',
		'INT',
		'CEM',
		'CCS',
		'EQE',
		'EC',
		'EF',
		'ED',
		'CET',
		'EE',
		'ETM',
		'ENV',
		'ENVT',
		'XMBA',
		'FE',
		'PA',
		'FLED',
		'GED',
		'GPH',
		'GUID',
		'HIST',
		'HUM',
		'IE',
		'INCT',
		'MIR',
		'MIR W/ THESIS',
		'INTT',
		'INTT',
		'LS',
		'LING',
		'AD',
		'MIS',
		'MATH',
		'SCED',
		'ME',
		'MECA',
		'BIO',
		'PHIL',
		'PE',
		'PHYS',
		'POLS',
		'PRED',
		'PSY',
		'YADYOK',
		'SCED',
		'SPL',
		'SOC',
		'SWE',
		'SWE W/ THESIS',
		'TRM',
		'SCO',
		'TRM',
		'WTR',
		'TR',
		'TK',
		'TKL',
		'LL'
	],
	names: [
		'ASIAN STUDIES',
		'ASIAN STUDIES WITH THESIS',
		'ATATURK INSTITUTE FOR MODERN TURKISH HISTORY',
		'AUTOMOTIVE ENGINEERING',
		'BIOMEDICAL ENGINEERING',
		'BUSINESS INFORMATION SYSTEMS',
		'CHEMICAL ENGINEERING',
		'CHEMISTRY',
		'CIVIL ENGINEERING',
		'COGNITIVE SCIENCE',
		'COMPUTATIONAL SCIENCE & ENGINEERING',
		'COMPUTER EDUCATION & EDUCATIONAL TECHNOLOGY',
		'COMPUTER ENGINEERING',
		'CONFERENCE INTERPRETING',
		'CONSTRUCTION ENGINEERING AND MANAGEMENT',
		'CRITICAL AND CULTURAL STUDIES',
		'EARTHQUAKE ENGINEERING',
		'ECONOMICS',
		'ECONOMICS AND FINANCE',
		'EDUCATIONAL SCIENCES',
		'EDUCATIONAL TECHNOLOGY',
		'ELECTRICAL & ELECTRONICS ENGINEERING',
		'ENGINEERING AND TECHNOLOGY MANAGEMENT',
		'ENVIRONMENTAL SCIENCES',
		'ENVIRONMENTAL TECHNOLOGY',
		'EXECUTIVE MBA',
		'FINANCIAL ENGINEERING',
		'FINE ARTS',
		'FOREIGN LANGUAGE EDUCATION',
		'GEODESY',
		'GEOPHYSICS',
		'GUIDANCE & PSYCHOLOGICAL COUNSELING',
		'HISTORY',
		'HUMANITIES COURSES COORDINATOR',
		'INDUSTRIAL ENGINEERING',
		'INTERNATIONAL COMPETITION AND TRADE',
		'INTERNATIONAL RELATIONS:TURKEY,EUROPE AND THE MIDDLE EAST',
		'INTERNATIONAL RELATIONS:TURKEY,EUROPE AND THE MIDDLE EAST WITH THESIS',
		'INTERNATIONAL TRADE',
		'INTERNATIONAL TRADE MANAGEMENT',
		'LEARNING SCIENCES',
		'LINGUISTICS',
		'MANAGEMENT',
		'MANAGEMENT INFORMATION SYSTEMS',
		'MATHEMATICS',
		'MATHEMATICS AND SCIENCE EDUCATION',
		'MECHANICAL ENGINEERING',
		'MECHATRONICS ENGINEERING (WITHOUT THESIS)',
		'MOLECULAR BIOLOGY & GENETICS',
		'PHILOSOPHY',
		'PHYSICAL EDUCATION',
		'PHYSICS',
		'POLITICAL SCIENCE&INTERNATIONAL RELATIONS',
		'PRIMARY EDUCATION',
		'PSYCHOLOGY',
		'SCHOOL OF FOREIGN LANGUAGES',
		'SECONDARY SCHOOL SCIENCE AND MATHEMATICS EDUCATION',
		'SOCIAL POLICY WITH THESIS',
		'SOCIOLOGY',
		'SOFTWARE ENGINEERING',
		'SOFTWARE ENGINEERING WITH THESIS',
		'SUSTAINABLE TOURISM MANAGEMENT',
		'SYSTEMS & CONTROL ENGINEERING',
		'TOURISM ADMINISTRATION',
		'TRANSLATION',
		'TRANSLATION AND INTERPRETING STUDIES',
		'TURKISH COURSES COORDINATOR',
		'TURKISH LANGUAGE & LITERATURE',
		'WESTERN LANGUAGES & LITERATURES'
	],
}

class CourseSelector {
	constructor(parentElement = document.body) {
		this.parent = parentElement;
		this.departments = DEPARTMENTS;
		this.init();
	}

	init() {
		this.createYearSelect();
		this.createSemesterSelect();
		this.createDepartmentSelect();
		this.createButtons();
		this.setupIframeContainer();
	}

	createYearSelect() {
		const titleYear = document.createElement('p');
		titleYear.textContent = 'Select Year';

		const selectYear = document.createElement('select');
		selectYear.id = 'year';

		const currentYear = new Date().getFullYear();
		const years = Array.from(
			{ length: currentYear - 1970 + 1 },
			(_, i) => currentYear - i
		);

		years.forEach(year => {
			const option = document.createElement('option');
			option.value = year;
			option.textContent = `${year}/${year + 1}`;
			selectYear.appendChild(option);
		});

		this.parent.append(titleYear, selectYear);
	}

	createSemesterSelect() {
		const titleSemester = document.createElement('p');
		titleSemester.textContent = 'Select Semester';

		const selectSemester = document.createElement('select');
		selectSemester.id = 'semester';

		const semesters = [
			{ value: '1', text: 'Fall - 1' },
			{ value: '2', text: 'Spring - 2' },
			{ value: '3', text: 'Summer - 3' }
		];

		semesters.forEach(({ value, text }) => {
			const option = document.createElement('option');
			option.value = value;
			option.textContent = text;
			selectSemester.appendChild(option);
		});

		this.parent.append(titleSemester, selectSemester);
	}

	createDepartmentSelect() {
		const titleDepartment = document.createElement('p');
		titleDepartment.textContent = 'Select Department';

		const selectDepartment = document.createElement('select');
		selectDepartment.id = 'department';

		this.departments.codes.forEach((code, index) => {
			const option = document.createElement('option');
			option.value = this.departments.urls[index];
			option.textContent = `${this.departments.names[index]} - ${code}`;
			selectDepartment.appendChild(option);
		});

		this.parent.append(titleDepartment, selectDepartment);
	}

	createButtons() {
		const buttonContainer = document.createElement('div');
		buttonContainer.className = 'button-container';
		buttonContainer.style.cssText = `
			display: flex;
			justify-content: center;
			align-items: center;
			gap: 10px;
			margin: 20px 0;
			width: 100%;
		`;

		const submitButton = document.createElement('button');
		submitButton.textContent = 'View Schedule';
		submitButton.id = 'submit';
		submitButton.addEventListener('click', () => this.handleSubmit());
	
		const openNewButton = document.createElement('button');
		openNewButton.textContent = 'Open in New Tab';
		openNewButton.id = 'open-new';
		openNewButton.addEventListener('click', () => this.handleOpenNew());
	
		buttonContainer.append(submitButton, openNewButton);
		this.parent.appendChild(buttonContainer);
	}

	async handleSubmit() {
		try {
			const year = document.getElementById('year').value;
			const semester = document.getElementById('semester').value;
			const department = document.getElementById('department').value;

			this.validateInputs(year, semester, department);

			const url = this.buildScheduleUrl(year, semester, department);
			await this.fetchSchedule(url);
		} catch (error) {
			this.showError(error.message);
		}
	}

	handleOpenNew() {
		const year = document.getElementById('year').value;
		const semester = document.getElementById('semester').value;
		const department = document.getElementById('department').value;

		this.validateInputs(year, semester, department);

		const url = this.buildScheduleUrl(year, semester, department);
		window.open(url, '_blank');

	}

	validateInputs(year, semester, department) {
		if (!year || !semester || !department) {
			throw new Error('Please fill in all fields');
		}

		const currentYear = new Date().getFullYear();
		if (year < 1970 || year > currentYear) {
			throw new Error('Invalid year selected');
		}
	}

	buildScheduleUrl(year, semester, department) {
		const baseUrl = 'https://registration.boun.edu.tr/scripts/sch.asp';

		return `${baseUrl}?donem=${year}/${parseInt(year) + 1}-${semester}&kisaadi=${department}`;
	}

	setupIframeContainer() {
    const container = document.createElement('div');
    container.id = 'schedule-container';
    container.style.cssText = `
      margin-top: 20px;
      width: 100%;
      height: 1000px;
      display: none;
    `;

    const iframe = document.createElement('iframe');
    iframe.id = 'schedule-frame';
    // Add sandbox attributes for security
    iframe.sandbox = 'allow-same-origin allow-scripts allow-forms';
    iframe.style.cssText = `
      width: 100%;
      height: 100%;
      border: 1px solid #ccc;
      border-radius: 4px;
    `;

    container.appendChild(iframe);
    this.parent.appendChild(container);
  }

  async fetchSchedule(url) {
    const button = document.getElementById('submit');
    const container = document.getElementById('schedule-container');
    const iframe = document.getElementById('schedule-frame');
    
    button.disabled = true;
    container.style.display = 'block';
    
    try {
      // Direct iframe loading instead of fetch
      iframe.style.opacity = '0.5';
      iframe.onload = () => {
        iframe.style.opacity = '1';
      };
      
      iframe.onerror = () => {
        this.showError('Failed to load schedule');
        container.style.display = 'none';
      };

      iframe.src = url;
      
    } catch (error) {
      this.showError(error.message);
      container.style.display = 'none';
    } finally {
      button.disabled = false;
    }
  }
	showError(message) {
		const errorDiv = document.createElement('div');
		errorDiv.role = 'alert';
		errorDiv.className = 'error-message';
		errorDiv.textContent = message;

		const existingError = document.querySelector('.error-message');
		if (existingError) {
			existingError.remove();
		}

		this.parent.appendChild(errorDiv);
		setTimeout(() => errorDiv.remove(), 3000);
	}
}

// Initialize the selector
const selector = new CourseSelector();