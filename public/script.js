var myParent = document.body;



// VARIABLES


const bolumArray = [
	'ASIA&bolum=ASIAN+STUDIES',
	'ASIA&bolum=ASIAN+STUDIES+WITH+THESIS',
	'ATA&bolum=ATATURK+INSTITUTE+FOR+MODERN+TURKISH+HISTORY',
	'AUTO&bolum=AUTOMOTIVE+ENGINEERING',
	'BM&bolum=BIOMEDICAL+ENGINEERING',
	'BIS&bolum=BUSINESS+INFORMATION+SYSTEMS',
	'CHE&bolum=CHEMICAL+ENGINEERING',
	'CHEM&bolum=CHEMISTRY',
	'CE&bolum=CIVIL+ENGINEERING',
	'COGS&bolum=COGNITIVE+SCIENCE',
	'CSE&bolum=COMPUTATIONAL+SCIENCE+%26+ENGINEERING',
	'CET&bolum=COMPUTER+EDUCATION+%26+EDUCATIONAL+TECHNOLOGY',
	'CMPE&bolum=COMPUTER+ENGINEERING',
	'INT&bolum=CONFERENCE+INTERPRETING',
	'CEM&bolum=CONSTRUCTION+ENGINEERING+AND+MANAGEMENT',
	'CCS&bolum=CRITICAL+AND+CULTURAL+STUDIES',
	'EQE&bolum=EARTHQUAKE+ENGINEERING',
	'EC&bolum=ECONOMICS',
	'EF&bolum=ECONOMICS+AND+FINANCE',
	'ED&bolum=EDUCATIONAL+SCIENCES',
	'CET&bolum=EDUCATIONAL+TECHNOLOGY',
	'EE&bolum=ELECTRICAL+%26+ELECTRONICS+ENGINEERING',
	'ETM&bolum=ENGINEERING+AND+TECHNOLOGY+MANAGEMENT',
	'ENV&bolum=ENVIRONMENTAL+SCIENCES',
	'ENVT&bolum=ENVIRONMENTAL+TECHNOLOGY',
	'XMBA&bolum=EXECUTIVE+MBA',
	'FE&bolum=FINANCIAL+ENGINEERING',
	'PA&bolum=FINE+ARTS',
	'FLED&bolum=FOREIGN+LANGUAGE+EDUCATION',
	'GED&bolum=GEODESY',
	'GPH&bolum=GEOPHYSICS',
	'GUID&bolum=GUIDANCE+%26+PSYCHOLOGICAL+COUNSELING',
	'HIST&bolum=HISTORY',
	'HUM&bolum=HUMANITIES+COURSES+COORDINATOR',
	'IE&bolum=INDUSTRIAL+ENGINEERING',
	'INCT&bolum=INTERNATIONAL+COMPETITION+AND+TRADE',
	'MIR&bolum=INTERNATIONAL+RELATIONS%3aTURKEY%2cEUROPE+AND+THE+MIDDLE+EAST',
	'MIR&bolum=INTERNATIONAL+RELATIONS%3aTURKEY%2cEUROPE+AND+THE+MIDDLE+EAST+WITH+THESIS',
	'INTT&bolum=INTERNATIONAL+TRADE',
	'INTT&bolum=INTERNATIONAL+TRADE+MANAGEMENT',
	'LS&bolum=LEARNING+SCIENCES',
	'LING&bolum=LINGUISTICS',
	'AD&bolum=MANAGEMENT',
	'MIS&bolum=MANAGEMENT+INFORMATION+SYSTEMS',
	'MATH&bolum=MATHEMATICS',
	'SCED&bolum=MATHEMATICS+AND+SCIENCE+EDUCATION',
	'ME&bolum=MECHANICAL+ENGINEERING',
	'MECA&bolum=MECHATRONICS+ENGINEERING+(WITHOUT+THESIS)',
	'BIO&bolum=MOLECULAR+BIOLOGY+%26+GENETICS',
	'PHIL&bolum=PHILOSOPHY',
	'PE&bolum=PHYSICAL+EDUCATION',
	'PHYS&bolum=PHYSICS',
	'POLS&bolum=POLITICAL+SCIENCE%26INTERNATIONAL+RELATIONS',
	'PRED&bolum=PRIMARY+EDUCATION',
	'PSY&bolum=PSYCHOLOGY',
	'YADYOK&bolum=SCHOOL+OF+FOREIGN+LANGUAGES',
	'SCED&bolum=SECONDARY+SCHOOL+SCIENCE+AND+MATHEMATICS+EDUCATION',
	'SPL&bolum=SOCIAL+POLICY+WITH+THESIS',
	'SOC&bolum=SOCIOLOGY',
	'SWE&bolum=SOFTWARE+ENGINEERING',
	'SWE&bolum=SOFTWARE+ENGINEERING+WITH+THESIS',
	'TRM&bolum=SUSTAINABLE+TOURISM+MANAGEMENT',
	'SCO&bolum=SYSTEMS+%26+CONTROL+ENGINEERING',
	'TRM&bolum=TOURISM+ADMINISTRATION',
	'WTR&bolum=TRANSLATION',
	'TR&bolum=TRANSLATION+AND+INTERPRETING+STUDIES',
	'TK&bolum=TURKISH+COURSES+COORDINATOR',
	'TKL&bolum=TURKISH+LANGUAGE+%26+LITERATURE',
	'LL&bolum=WESTERN+LANGUAGES+%26+LITERATURES'
]

let bolumKodArray = [
	'ASIA',
	'ASIA W/ THESIS',
	'ATA',
	'AUTO',
	'BM',
	'BIS',
	'CHE',
	'CHEM',
	'CE',
	'COGS',
	'CSE',
	'CET',
	'CMPE',
	'INT',
	'CEM',
	'CCS',
	'EQE',
	'EC',
	'EF',
	'ED',
	'CET',
	'EE',
	'ETM',
	'ENV',
	'ENVT',
	'XMBA',
	'FE',
	'PA',
	'FLED',
	'GED',
	'GPH',
	'GUID',
	'HIST',
	'HUM',
	'IE',
	'INCT',
	'MIR',
	'MIR W/ THESIS',
	'INTT',
	'INTT',
	'LS',
	'LING',
	'AD',
	'MIS',
	'MATH',
	'SCED',
	'ME',
	'MECA',
	'BIO',
	'PHIL',
	'PE',
	'PHYS',
	'POLS',
	'PRED',
	'PSY',
	'YADYOK',
	'SCED',
	'SPL',
	'SOC',
	'SWE',
	'SWE W/ THESIS',
	'TRM',
	'SCO',
	'TRM',
	'WTR',
	'TR',
	'TK',
	'TKL',
	'LL'
]

let bolumIsimArray = [
	'ASIAN STUDIES',
	'ASIAN STUDIES WITH THESIS',
	'ATATURK INSTITUTE FOR MODERN TURKISH HISTORY',
	'AUTOMOTIVE ENGINEERING',
	'BIOMEDICAL ENGINEERING',
	'BUSINESS INFORMATION SYSTEMS',
	'CHEMICAL ENGINEERING',
	'CHEMISTRY',
	'CIVIL ENGINEERING',
	'COGNITIVE SCIENCE',
	'COMPUTATIONAL SCIENCE & ENGINEERING',
	'COMPUTER EDUCATION & EDUCATIONAL TECHNOLOGY',
	'COMPUTER ENGINEERING',
	'CONFERENCE INTERPRETING',
	'CONSTRUCTION ENGINEERING AND MANAGEMENT',
	'CRITICAL AND CULTURAL STUDIES',
	'EARTHQUAKE ENGINEERING',
	'ECONOMICS',
	'ECONOMICS AND FINANCE',
	'EDUCATIONAL SCIENCES',
	'EDUCATIONAL TECHNOLOGY',
	'ELECTRICAL & ELECTRONICS ENGINEERING',
	'ENGINEERING AND TECHNOLOGY MANAGEMENT',
	'ENVIRONMENTAL SCIENCES',
	'ENVIRONMENTAL TECHNOLOGY',
	'EXECUTIVE MBA',
	'FINANCIAL ENGINEERING',
	'FINE ARTS',
	'FOREIGN LANGUAGE EDUCATION',
	'GEODESY',
	'GEOPHYSICS',
	'GUIDANCE & PSYCHOLOGICAL COUNSELING',
	'HISTORY',
	'HUMANITIES COURSES COORDINATOR',
	'INDUSTRIAL ENGINEERING',
	'INTERNATIONAL COMPETITION AND TRADE',
	'INTERNATIONAL RELATIONS:TURKEY,EUROPE AND THE MIDDLE EAST',
	'INTERNATIONAL RELATIONS:TURKEY,EUROPE AND THE MIDDLE EAST WITH THESIS',
	'INTERNATIONAL TRADE',
	'INTERNATIONAL TRADE MANAGEMENT',
	'LEARNING SCIENCES',
	'LINGUISTICS',
	'MANAGEMENT',
	'MANAGEMENT INFORMATION SYSTEMS',
	'MATHEMATICS',
	'MATHEMATICS AND SCIENCE EDUCATION',
	'MECHANICAL ENGINEERING',
	'MECHATRONICS ENGINEERING (WITHOUT THESIS)',
	'MOLECULAR BIOLOGY & GENETICS',
	'PHILOSOPHY',
	'PHYSICAL EDUCATION',
	'PHYSICS',
	'POLITICAL SCIENCE&INTERNATIONAL RELATIONS',
	'PRIMARY EDUCATION',
	'PSYCHOLOGY',
	'SCHOOL OF FOREIGN LANGUAGES',
	'SECONDARY SCHOOL SCIENCE AND MATHEMATICS EDUCATION',
	'SOCIAL POLICY WITH THESIS',
	'SOCIOLOGY',
	'SOFTWARE ENGINEERING',
	'SOFTWARE ENGINEERING WITH THESIS',
	'SUSTAINABLE TOURISM MANAGEMENT',
	'SYSTEMS & CONTROL ENGINEERING',
	'TOURISM ADMINISTRATION',
	'TRANSLATION',
	'TRANSLATION AND INTERPRETING STUDIES',
	'TURKISH COURSES COORDINATOR',
	'TURKISH LANGUAGE & LITERATURE',
	'WESTERN LANGUAGES & LITERATURES'
]




// YEAR PART

// Year selector title
let titleYear = document.createElement("p");
titleYear.innerText = 'Select Year';
myParent.appendChild(titleYear);


// Select element creation
var selectYear = document.createElement("select");
selectYear.id = "year";
myParent.appendChild(selectYear);

// Create an array of years
let thisYear = new Date().getFullYear();
let yearArray = [];
for (let i = 1970; i <= thisYear; i++) {
	yearArray.push(i);
}

// Create and append the options
for (var i = yearArray.length - 1; i >= 0; i--) {
	var option = document.createElement("option");
	option.value = yearArray[i];
	option.text = yearArray[i] + '/' + (yearArray[i] + 1);
	selectYear.appendChild(option);
}


// SEMESTER PART
let semesterTitle = document.createElement("p");
semesterTitle.innerText = 'Select Semester';
myParent.appendChild(semesterTitle);

var semesterSelect = document.createElement("select");
semesterSelect.id = "term";
myParent.appendChild(semesterSelect);

for (var i = 1; i < 4; i++) {
	var option = document.createElement("option");
	option.value = i;
	option.text = i;
	semesterSelect.appendChild(option);
}




// DEPARTMENT PART

let depAbbrTitle = document.createElement("p");
depAbbrTitle.innerText = 'Select Department Abbr.';
myParent.appendChild(depAbbrTitle);

var depAbbrSelect = document.createElement("select");
depAbbrSelect.id = "bolum";
myParent.appendChild(depAbbrSelect);

//Create and append the options
for (var i = 0; i < bolumArray.length; i++) {
	var option = document.createElement("option");
	option.value = bolumArray[i];
	option.text = bolumKodArray[i];
	depAbbrSelect.appendChild(option);
}


let depNameTitle = document.createElement("p");
depNameTitle.innerText = 'or Select Department Name.';
myParent.appendChild(depNameTitle);

var deptNameSelect = document.createElement("select");
deptNameSelect.id = "bolumIsim";
myParent.appendChild(deptNameSelect);

for (var i = 0; i < bolumArray.length; i++) {
	var option = document.createElement("option");
	option.value = bolumArray[i];
	option.text = bolumIsimArray[i];
	deptNameSelect.appendChild(option);
}

depAbbrSelect.addEventListener("change", function () {
	deptNameSelect.value = depAbbrSelect.value;
});

deptNameSelect.addEventListener("change", function () {
	depAbbrSelect.value = deptNameSelect.value;
});


const urlHead = 'https://registration.boun.edu.tr/scripts/sch.asp?donem=';
const urlMid = '&kisaadi=';

var button = document.createElement("button");
button.id = 'submit';
button.innerText = 'Click to go schedule';
button.type = 'button';
button.addEventListener('click', function () {
	try {
			const year = document.getElementById("year").value;
			const term = document.getElementById("term").value;
			const department = document.getElementById("bolum").value;
			
			if (!year || !term || !department) {
					alert("Please select all required fields");
					return;
			}
			
			const urlString = `${urlHead}${year}/${parseInt(year) + 1}-${term}${urlMid}${department}`;
			window.open(urlString, '_blank');
	} catch (err) {
			console.error("Error generating URL:", err);
			alert("Something went wrong. Please try again.");
	}
});
myParent.appendChild(button);

document.addEventListener('keydown', function(e) {
	if (e.key === 'Enter' && e.target.tagName === 'SELECT') {
			button.click();
	}
});